USE [quanlykhachsan]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 09/04/2023 8:48:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[uID] [int] IDENTITY(1,1) NOT NULL,
	[user] [varchar](255) NULL,
	[pass] [varchar](255) NULL,
	[isSell] [int] NULL,
	[isAdmin] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[uID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[booking]    Script Date: 09/04/2023 8:48:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[booking](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[u_id] [int] NULL,
	[pid] [int] NULL,
	[uname] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[phone] [nvarchar](max) NULL,
	[checkin] [nvarchar](max) NULL,
	[checkout] [nvarchar](max) NULL,
	[songay] [int] NULL,
	[hoadon] [int] NULL,
	[mess] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[login]    Script Date: 09/04/2023 8:48:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[login](
	[user] [nchar](10) NULL,
	[pass] [nchar](10) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[product]    Script Date: 09/04/2023 8:48:21 CH ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[product](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[image] [nvarchar](max) NULL,
	[price] [int] NULL,
	[title] [nvarchar](max) NULL,
	[description] [nvarchar](max) NULL,
	[cateID] [int] NULL,
	[sell_ID] [int] NULL,
	[number] [int] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Account] ON 

INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (1, N'Adam', N'123456', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (3, N'Ferris', N'RXH3XJ', 0, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (4, N'Katell', N'HWV8ZN', 0, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (5, N'Zahir', N'NPX7OF', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (6, N'Conan', N'WIZ5VZ', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (7, N'Cade', N'ZSW2LU', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (8, N'Micah', N'RVV5SR', 0, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (9, N'Rowan', N'VAI6XR', 1, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (10, N'Kirby', N'DNX6JK', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (11, N'Tanisha', N'XWU7JP', 0, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (12, N'Howard', N'TSR5MR', 0, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (13, N'hoa', N'hoa', 0, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (14, N'Hadassah', N'YOY7ZW', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (15, N'Echo', N'IGE8TN', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (16, N'Slade', N'OFO6QS', 0, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (17, N'Devin', N'IBM6RZ', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (18, N'Rowan', N'ZYS9VI', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (19, N'Rafael', N'WZA0IH', 1, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (20, N'Madaline', N'QMW4EN', 0, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (21, N'Vera', N'CZB2VM', 0, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (22, N'Declan', N'ZKE7QZ', 1, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (23, N'Katell', N'SFS0IW', 1, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (24, N'Summer', N'PSQ7LC', 0, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (25, N'Robin', N'KIS9AF', 1, 1)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (26, N'Dominique', N'IKV0IX', 0, 0)
INSERT [dbo].[Account] ([uID], [user], [pass], [isSell], [isAdmin]) VALUES (27, N'admin', N'123', 1, 1)
SET IDENTITY_INSERT [dbo].[Account] OFF
GO
SET IDENTITY_INSERT [dbo].[booking] ON 

INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (7, 0, 9, N'vxh', N'vuxuanhoa2403@gmail.com', N'0362943381', N'2023-04-12', N'2023-04-21', 9, 1620000, N'cxzxc')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (8, 0, 9, N'', N'', N'', N'2023-04-15', N'2023-04-16', 1, 180000, N'sdvsv')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (9, 0, 9, N'', N'', N'', N'2023-04-15', N'2023-04-16', 1, 180000, N'sdvsv')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (10, 0, 9, N'', N'', N'', N'2023-04-15', N'2023-04-16', 1, 180000, N'sdvsv')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (11, 0, 9, N'', N'', N'', N'2023-04-15', N'2023-04-16', 1, 180000, N'sdvsv')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (12, 1, 2, N'vxh', N'vuxuanhoa2403@gmail.com', N'0362943381', N'2023-04-10', N'2023-04-15', 5, 600000, N'adam')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (13, 1, 24, N'hello', N'vuxuanhiep2403@gmail.com', N'3333333333', N'2023-04-04', N'2023-04-10', 6, 1200000, N'ok')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (14, 1, 8, N'vu hoa', N'vuxuanhoa240322@gmail.com', N'3333', N'2023-04-06', N'2023-04-09', 3, 450000, N'thanks')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (15, 1, 8, N'vu hiep', N'vuxuanhoa240322@gmail.com', N'686868', N'2023-04-04', N'2023-04-08', 4, 600000, N'svfbgb')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (16, 1, 8, N'vu hiep', N'vuxuanhoa240322@gmail.com', N'686868', N'2023-04-04', N'2023-04-08', 4, 600000, N'svfbgb')
INSERT [dbo].[booking] ([id], [u_id], [pid], [uname], [email], [phone], [checkin], [checkout], [songay], [hoadon], [mess]) VALUES (17, 13, 8, N'vu hoa', N'vuxuanhoa240322@gmail.com', N'222222', N'2023-04-03', N'2023-04-13', 10, 1500000, N'ok')
SET IDENTITY_INSERT [dbo].[booking] OFF
GO
INSERT [dbo].[login] ([user], [pass]) VALUES (N'abc       ', N'123       ')
INSERT [dbo].[login] ([user], [pass]) VALUES (N'123       ', N'123       ')
GO
SET IDENTITY_INSERT [dbo].[product] ON 

INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (1, N'Single Room', N'https://www.airahotelbangkok.com/wp-content/uploads/2021/11/Deluxe-Suite_Living-Room_1.jpg', 100000, N'TiÃÂªu chuÃ¡ÂºÂ©n 5 sao', N'MÃÂ°Ã¡Â»Âng Thanh PhÃÂº QuÃ¡Â»Âc lÃÂ  khÃÂ¡ch sÃ¡ÂºÂ¡n thuÃ¡Â»Âc MÃÂ°Ã¡Â»Âng Thanh Hospitality Ã¢ÂÂ mÃ¡Â»Ât trong nhÃ¡Â»Â¯ng chuÃ¡Â»Âi khÃÂ¡ch sÃ¡ÂºÂ¡n tÃÂ° nhÃÂ¢n lÃ¡Â»Ân nhÃ¡ÂºÂ¥t nhÃÂ¬ ViÃ¡Â»Ât Nam. NÃ¡ÂºÂ±m tÃ¡ÂºÂ¡i BÃÂ£i TrÃÂ°Ã¡Â»Âng, khÃÂ¡ch sÃ¡ÂºÂ¡n MÃÂ°Ã¡Â»Âng Thanh Luxury PhÃÂº QuÃ¡Â»Âc Hotel lÃÂ  ÃÂÃ¡Â»Âa ÃÂiÃ¡Â»Âm nghÃ¡Â»Â dÃÂ°Ã¡Â»Â¡ng ÃÂÃÂ°Ã¡Â»Â£c ÃÂÃÂ¡nh giÃÂ¡ cÃÂ³ chÃ¡ÂºÂ¥t lÃÂ°Ã¡Â»Â£ng 5 sao vÃ¡Â»Âi cÃÂ¡ sÃ¡Â»Â vÃ¡ÂºÂ­t chÃ¡ÂºÂ¥t khang trang vÃÂ  hiÃ¡Â»Ân ÃÂÃ¡ÂºÂ¡i, thÃÂªm vÃÂ o ÃÂÃÂ³ lÃÂ  hÃÂ ng loÃ¡ÂºÂ¡t tiÃ¡Â»Ân nghi sang trÃ¡Â»Âng, chÃ¡ÂºÂ¯c chÃ¡ÂºÂ¯n ÃÂÃÂ¡p Ã¡Â»Â©ng nhu cÃ¡ÂºÂ§u ÃÂa dÃ¡ÂºÂ¡ng cÃ¡Â»Â§a bÃ¡ÂºÂ¡n.NÃ¡ÂºÂ±m sÃÂ¡t bÃÂ£i biÃ¡Â»Ân xinh ÃÂÃ¡ÂºÂ¹p, khi nghÃ¡Â»Â ngÃÂ¡i tÃ¡ÂºÂ¡i ÃÂÃÂ¢y bÃ¡ÂºÂ¡n cÃÂ³ thÃ¡Â»Â dÃ¡Â»Â dÃÂ ng ngÃ¡ÂºÂ¯m nhÃÂ¬n toÃÂ n cÃ¡ÂºÂ£nh ÃÂÃ¡ÂºÂ£o NgÃ¡Â»Âc rÃ¡Â»Â±c rÃ¡Â»Â¡ sÃ¡ÂºÂ¯c mÃÂ u vÃÂ  tÃ¡ÂºÂ­n hÃÂ°Ã¡Â»Âng khÃÂ´ng khÃÂ­ trong lÃÂ nh, dÃ¡Â»Â chÃ¡Â»Âu nhÃ¡ÂºÂ¥t. BÃÂªn cÃ¡ÂºÂ¡nh ÃÂÃÂ³, chÃ¡ÂºÂ¥t lÃÂ°Ã¡Â»Â£ng dÃ¡Â»Âch vÃ¡Â»Â¥ tÃ¡ÂºÂ¡i khÃÂ¡ch sÃ¡ÂºÂ¡n sÃ¡ÂºÂ½ lÃÂ m bÃ¡ÂºÂ¡n cÃÂ ng thÃÂªm hÃÂ i lÃÂ²ng, khoÃ¡ÂºÂ£ng thÃ¡Â»Âi gian nghÃ¡Â»Â dÃÂ°Ã¡Â»Â¡ng cÃ¡Â»Â§a bÃ¡ÂºÂ¡n sÃ¡ÂºÂ½ trÃ¡Â»Â nÃÂªn thÃÂ° giÃÂ£n hÃÂ¡n bao giÃ¡Â»Â hÃ¡ÂºÂ¿t.', 1, 1, 2)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (8, N'Double Room', N'https://www.airahotelbangkok.com/wp-content/uploads/2021/11/Deluxe-Suite_Living-Room_1.jpg', 150000, N'Tiêu chuẩn 5 sao', N'Mường Thanh Phú Quốc là khách sạn thuộc Mường Thanh Hospitality – một trong những chuỗi khách sạn tư nhân lớn nhất nhì Việt Nam. Nằm tại Bãi Trường, khách sạn Mường Thanh Luxury Phú Quốc Hotel là địa điểm nghỉ dưỡng được đánh giá có chất lượng 5 sao với cơ sở vật chất khang trang và hiện đại, thêm vào đó là hàng loạt tiện nghi sang trọng, chắc chắn đáp ứng nhu cầu đa dạng của bạn.Nằm sát bãi biển xinh đẹp, khi nghỉ ngơi tại đây bạn có thể dễ dàng ngắm nhìn toàn cảnh Đảo Ngọc rực rỡ sắc màu và tận hưởng không khí trong lành, dễ chịu nhất. Bên cạnh đó, chất lượng dịch vụ tại khách sạn sẽ làm bạn càng thêm hài lòng, khoảng thời gian nghỉ dưỡng của bạn sẽ trở nên thư giãn hơn bao giờ hết.', 4, 15, 2)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (9, N'Quad Room', N'https://www.airahotelbangkok.com/wp-content/uploads/2021/11/Deluxe-Suite_Living-Room_1.jpg', 180000, N'Tiêu chuẩn 5 sao', N'Mường Thanh Phú Quốc là khách sạn thuộc Mường Thanh Hospitality – một trong những chuỗi khách sạn tư nhân lớn nhất nhì Việt Nam. Nằm tại Bãi Trường, khách sạn Mường Thanh Luxury Phú Quốc Hotel là địa điểm nghỉ dưỡng được đánh giá có chất lượng 5 sao với cơ sở vật chất khang trang và hiện đại, thêm vào đó là hàng loạt tiện nghi sang trọng, chắc chắn đáp ứng nhu cầu đa dạng của bạn.Nằm sát bãi biển xinh đẹp, khi nghỉ ngơi tại đây bạn có thể dễ dàng ngắm nhìn toàn cảnh Đảo Ngọc rực rỡ sắc màu và tận hưởng không khí trong lành, dễ chịu nhất. Bên cạnh đó, chất lượng dịch vụ tại khách sạn sẽ làm bạn càng thêm hài lòng, khoảng thời gian nghỉ dưỡng của bạn sẽ trở nên thư giãn hơn bao giờ hết.', 3, 14, 1)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (24, N'Single Room', N'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzC7bstwyt0-Fm3vcwiwS8rZiXbzGF1SuY_MXGSQS0xnRQ84FlMEilVMdt7aa9eDed20A&usqp=CAU', 200000, N'5s', N'ok', 1, 1, 3)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (3, N'Quad Room', N'https://www.airahotelbangkok.com/wp-content/uploads/2021/11/Deluxe-Suite_Living-Room_1.jpg', 130000, N'Tiêu chuẩn 5 sao', N'Mường Thanh Phú Quốc là khách sạn thuộc Mường Thanh Hospitality – một trong những chuỗi khách sạn tư nhân lớn nhất nhì Việt Nam. Nằm tại Bãi Trường, khách sạn Mường Thanh Luxury Phú Quốc Hotel là địa điểm nghỉ dưỡng được đánh giá có chất lượng 5 sao với cơ sở vật chất khang trang và hiện đại, thêm vào đó là hàng loạt tiện nghi sang trọng, chắc chắn đáp ứng nhu cầu đa dạng của bạn.Nằm sát bãi biển xinh đẹp, khi nghỉ ngơi tại đây bạn có thể dễ dàng ngắm nhìn toàn cảnh Đảo Ngọc rực rỡ sắc màu và tận hưởng không khí trong lành, dễ chịu nhất. Bên cạnh đó, chất lượng dịch vụ tại khách sạn sẽ làm bạn càng thêm hài lòng, khoảng thời gian nghỉ dưỡng của bạn sẽ trở nên thư giãn hơn bao giờ hết.', 1, 7, 2)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (4, N'Twin Room', N'https://www.airahotelbangkok.com/wp-content/uploads/2021/11/Deluxe-Suite_Living-Room_1.jpg', 140000, N'Tiêu chuẩn 5 sao', N'Mường Thanh Phú Quốc là khách sạn thuộc Mường Thanh Hospitality – một trong những chuỗi khách sạn tư nhân lớn nhất nhì Việt Nam. Nằm tại Bãi Trường, khách sạn Mường Thanh Luxury Phú Quốc Hotel là địa điểm nghỉ dưỡng được đánh giá có chất lượng 5 sao với cơ sở vật chất khang trang và hiện đại, thêm vào đó là hàng loạt tiện nghi sang trọng, chắc chắn đáp ứng nhu cầu đa dạng của bạn.Nằm sát bãi biển xinh đẹp, khi nghỉ ngơi tại đây bạn có thể dễ dàng ngắm nhìn toàn cảnh Đảo Ngọc rực rỡ sắc màu và tận hưởng không khí trong lành, dễ chịu nhất. Bên cạnh đó, chất lượng dịch vụ tại khách sạn sẽ làm bạn càng thêm hài lòng, khoảng thời gian nghỉ dưỡng của bạn sẽ trở nên thư giãn hơn bao giờ hết.', 2, 1, 2)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (10, N'Double Room', N'https://www.airahotelbangkok.com/wp-content/uploads/2021/11/Deluxe-Suite_Living-Room_1.jpg', 180000, N'Tiêu chuẩn 5 sao', N'Mường Thanh Phú Quốc là khách sạn thuộc Mường Thanh Hospitality – một trong những chuỗi khách sạn tư nhân lớn nhất nhì Việt Nam. Nằm tại Bãi Trường, khách sạn Mường Thanh Luxury Phú Quốc Hotel là địa điểm nghỉ dưỡng được đánh giá có chất lượng 5 sao với cơ sở vật chất khang trang và hiện đại, thêm vào đó là hàng loạt tiện nghi sang trọng, chắc chắn đáp ứng nhu cầu đa dạng của bạn.Nằm sát bãi biển xinh đẹp, khi nghỉ ngơi tại đây bạn có thể dễ dàng ngắm nhìn toàn cảnh Đảo Ngọc rực rỡ sắc màu và tận hưởng không khí trong lành, dễ chịu nhất. Bên cạnh đó, chất lượng dịch vụ tại khách sạn sẽ làm bạn càng thêm hài lòng, khoảng thời gian nghỉ dưỡng của bạn sẽ trở nên thư giãn hơn bao giờ hết.', 4, 15, 2)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (11, N'Triple Room
', N'https://www.hotelgrandsaigon.com/wp-content/uploads/sites/227/2017/12/GRAND_PDLK_01.jpg', 150000, N'Tiêu chuẩn 5 sao', N'Mường Thanh Phú Quốc là khách sạn thuộc Mường Thanh Hospitality – một trong những chuỗi khách sạn tư nhân lớn nhất nhì Việt Nam. Nằm tại Bãi Trường, khách sạn Mường Thanh Luxury Phú Quốc Hotel là địa điểm nghỉ dưỡng được đánh giá có chất lượng 5 sao với cơ sở vật chất khang trang và hiện đại, thêm vào đó là hàng loạt tiện nghi sang trọng, chắc chắn đáp ứng nhu cầu đa dạng của bạn.Nằm sát bãi biển xinh đẹp, khi nghỉ ngơi tại đây bạn có thể dễ dàng ngắm nhìn toàn cảnh Đảo Ngọc rực rỡ sắc màu và tận hưởng không khí trong lành, dễ chịu nhất. Bên cạnh đó, chất lượng dịch vụ tại khách sạn sẽ làm bạn càng thêm hài lòng, khoảng thời gian nghỉ dưỡng của bạn sẽ trở nên thư giãn hơn bao giờ hết.', 3, 17, 2)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (27, N'Double Room', N'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxPbSOVASu1cyhkaog6hHQGNhpX5X84okJiytacj0j9zVy__nPWE9B6XXuWhyJoiFiS0Q&usqp=CAU', 400000, N'2 sao', N'aaaaa', 1, 1, 2)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (5, N'Quad Room', N'https://www.airahotelbangkok.com/wp-content/uploads/2021/11/Deluxe-Suite_Living-Room_1.jpg', 150000, N'Tiêu chuẩn 5 sao', N'Mường Thanh Phú Quốc là khách sạn thuộc Mường Thanh Hospitality – một trong những chuỗi khách sạn tư nhân lớn nhất nhì Việt Nam. Nằm tại Bãi Trường, khách sạn Mường Thanh Luxury Phú Quốc Hotel là địa điểm nghỉ dưỡng được đánh giá có chất lượng 5 sao với cơ sở vật chất khang trang và hiện đại, thêm vào đó là hàng loạt tiện nghi sang trọng, chắc chắn đáp ứng nhu cầu đa dạng của bạn.Nằm sát bãi biển xinh đẹp, khi nghỉ ngơi tại đây bạn có thể dễ dàng ngắm nhìn toàn cảnh Đảo Ngọc rực rỡ sắc màu và tận hưởng không khí trong lành, dễ chịu nhất. Bên cạnh đó, chất lượng dịch vụ tại khách sạn sẽ làm bạn càng thêm hài lòng, khoảng thời gian nghỉ dưỡng của bạn sẽ trở nên thư giãn hơn bao giờ hết.', 1, 6, 4)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (25, N'Twin Room', N'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSzC7bstwyt0-Fm3vcwiwS8rZiXbzGF1SuY_MXGSQS0xnRQ84FlMEilVMdt7aa9eDed20A&usqp=CAU', 100000, N'4 sao', N'abc', 1, 1, 4)
INSERT [dbo].[product] ([id], [name], [image], [price], [title], [description], [cateID], [sell_ID], [number]) VALUES (26, N'Quad Room', N'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTxPbSOVASu1cyhkaog6hHQGNhpX5X84okJiytacj0j9zVy__nPWE9B6XXuWhyJoiFiS0Q&usqp=CAU', 300, N'7 sao', N'dep', 1, 1, NULL)
SET IDENTITY_INSERT [dbo].[product] OFF
GO
